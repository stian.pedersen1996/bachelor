package norsvin.exceptions.alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * The type Alert message.
 */
public final class AlertMessage {

    /**
     * Constant containing resource path.
     */
    private static final String BASE_LOCATION = "i18n.Language";

    /**
     * Shows alerts in different languages. Key is getting sent in.
     *
     * @param keyTitle   Title of the confirm alert box.
     * @param keyHeader  Description of what is happening.
     * @param keyContent Context of what you are confirming
     * @param tato       Tatonumber involved.
     * @return Returns true or false if response is OK or no.
     */
    public static boolean confirmDelete(String keyTitle, String keyHeader, String keyContent, String tato) {
        //Language bundle, i18n. Finds users pref. language
        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());
        String titleText = bundle.getString(keyTitle);
        String headerText = bundle.getString(keyHeader);
        String contentText = bundle.getString(keyContent);
        String yesText = bundle.getString("norsvin.yes");
        String noText = bundle.getString("norsvin.no");

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleText);
        alert.setHeaderText(headerText +"< " + tato + " >");
        alert.setContentText(contentText);

        ButtonType okButton = new ButtonType(yesText);
        ButtonType noButton = new ButtonType(noText);
        alert.getButtonTypes().setAll(okButton, noButton);

        Optional<ButtonType> result = alert.showAndWait();
        return result.filter(buttonType -> buttonType == okButton).isPresent();


    }

    /**
     * Shows alerts in different languages. Key is getting sent in.
     *
     * @param keyTitle   Title of the confirm alert box.
     * @param keyHeader  Description of what is happening.
     * @param keyContent Context of what is being alerted
     * @param tato       Tatonumber involved.
     */
    public static void showAlert(String keyTitle, String keyHeader, String keyContent, String tato) {

        //Language bundle, i18n. Finds users pref. language
        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());
        String titleText = bundle.getString(keyTitle);
        String headerText = bundle.getString(keyHeader);
        String contentText = bundle.getString(keyContent);

        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(titleText);
        alert.setHeaderText(headerText + "< " + tato + " >");
        alert.setContentText(contentText);

        alert.showAndWait();
    }


    /**
     * Shows alerts in different languages. Key is getting sent in.
     *
     * @param keyTitle   Title of the confirm alert box.
     * @param keyHeader  Description of what is happening.
     * @param keyContent Context of what is being alerted.
     * @return the alert
     */
    public static Alert selectScale(String keyTitle, String keyHeader, String keyContent) {
        //Language bundle, i18n. Finds users pref. language
        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());
        String titleText = bundle.getString(keyTitle);
        String headerText = bundle.getString(keyHeader);
        String contentText = bundle.getString(keyContent);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(titleText);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        return alert;

    }


    /**
     * Shows alerts in different languages. Key is getting sent in.
     *
     * @param keyTitle   Title of the confirm alert box.
     * @param keyHeader  Description of what is happening.
     * @param keyContent Context of what is being alerted
     */
    public static void showAlert(String keyTitle, String keyHeader, String keyContent) {

        //Language bundle, i18n. Finds users pref. language
        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());
        String titleText = bundle.getString(keyTitle);
        String headerText = bundle.getString(keyHeader);
        String contentText = bundle.getString(keyContent);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(titleText);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.showAndWait();
    }

}
