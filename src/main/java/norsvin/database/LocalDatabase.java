package norsvin.database;

import javafx.collections.ObservableList;
import norsvin.gui.NorsvinController;
import norsvin.logic.csv.CsvHandler;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Local database.
 */
public class LocalDatabase {


    /**
     * Method connects to local Database
     *
     * @return Returns a connection session with the database. Will return NULL if the connection fails.
     */
    public static Connection connect() {

        Connection con = null;

        //Tries to get local database.
        // For sqllite
        try {

            //Sets database driver/engine.
            Class.forName("org.sqlite.JDBC");

            //Fetches connection to the database
            con = DriverManager.getConnection("jdbc:sqlite:localDatabase.db");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return con;
    }


    /**
     * Method will try to generate new tables for the database.
     * This method acts as a failsafe for the application in the event that that the database gets deleted.
     */
    public static void makeNewDB(){
        try {

            //Prepares Sql statement for creating new table.
            String sql = """
                CREATE TABLE identity
                (
                    tatonumber varchar not null
                        constraint identity_pk
                            primary key,
                    rfid       varchar,
                    location   int,
                    race       varchar,
                    sex        varchar,
                    born       date,
                    deceased   boolean default false not null
                );
                
                """;

            Connection con = connect();

            //Makes a statement, which can send SQL querys to the database.
            Statement stmt  = con.createStatement();

            //Executes the query.
            stmt.executeUpdate(sql);
            sql = """
                create table weighed
                (
                    tatonumber varchar  not null ,
                    weigh_date date     not null ,
                    weight     float    not null,
                    exported   boolean  default false not null,
                    FOREIGN KEY (tatonumber)
                        REFERENCES identity(tatonumber),
                    constraint weighed_pk
                        primary key (tatonumber, weigh_date)
                );
                """;
            stmt.executeUpdate(sql);
            sql = """
                create unique index identity_rfid_uindex
                    on identity (rfid);

                create unique index identity_tatonumber_uindex
                    on identity (tatonumber);
                """;
            stmt.executeUpdate(sql);
            // loop through the result set
            stmt.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * Searches the database for objects where tatonumber or rfid equals the parameter.
     *
     * @param query String used to check tatonumber and RFID.
     * @return Boolean : Returns true if the query has a match in the db, false if no matches are found.
     */
    public static boolean identityExists(String query){
        String sql = """
            SELECT identity.tatonumber
            FROM identity WHERE identity.tatonumber LIKE ? OR rfid LIKE ?
            """;
        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            //search from ending and upwards.
            pstmt.setString(1,  query);
            pstmt.setString(2, query);
            return pstmt.executeQuery().next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Searches the database for objects where tatonumber or rfid equals the parameter.
     *
     * @param query String used to check tatonumber and RFID.
     * @param date  the date
     * @return Boolean : Returns true if the query has a match in the db, false if no matches are found.
     */
    public static boolean weighedExists(String query, LocalDate date){
        String sql = """
            SELECT weighed.tatonumber
            FROM weighed
            WHERE weighed.tatonumber = ? and weighed.weigh_date = ?
            """;
        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            //search from ending and upwards.
            pstmt.setString(1,  query);
            pstmt.setDate(2, Date.valueOf(date));
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()){
                System.err.println("fant ein: " + rs.getString("tatonumber"));
                return true;
            }
            System.err.println("it ain't here bruh");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    private static boolean hasWeight(String query){
        String sql = """
            SELECT weighed.tatonumber
            FROM weighed WHERE weighed.tatonumber = ?
            """;
        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            //search from ending and upwards.
            pstmt.setString(1,  query);
            return pstmt.executeQuery().next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Attempts to insert a new object into the Identity table.
     *
     * @param tato     String:  This parameter will be inserted to the database as tatonumber.
     * @param rfid     String:  This parameter will be inserted to the database as rfid.
     * @param location String:  This parameter will be inserted to the database as location.
     * @param sex      String:  This parameter will be inserted to the database as sex.
     * @param race     String:  This parameter will be inserted to the database as race.
     * @param born     the born
     * @return Returns : true if insertion is successful and false if it fails.
     * @throws SQLException the sql exception
     */
    public static boolean attemptInsertIdentity(String tato, String rfid, String location, String sex,
                                                String race, LocalDate born) throws SQLException {
        if (identityExists(tato)){ return false;}
        if (!regexPass(tato)){return false;}
        try {
            Connection con = LocalDatabase.connect();
            //replaces the ? in the sql string at the correlated index with the parameter provided.
            if (location != null || born != null) {
                String sql = """
                    insert into main.identity (tatonumber, rfid, location, sex, race, born)
                    values  (?, ?, ?, ?, ?, ?);
                    """;
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.setString(1, tato);
                pstmt.setString(2, rfid);
                pstmt.setInt(3, Integer.parseInt(location));
                pstmt.setString(4, sex);
                pstmt.setString(5, race);
                pstmt.setDate(6, Date.valueOf(born));
                pstmt.executeUpdate();
            }else {
                String sql = """
                    insert into main.identity (tatonumber)
                    values  (?);
                    """;
                PreparedStatement pstmt = con.prepareStatement(sql);
                pstmt.setString(1, tato);
                pstmt.executeUpdate();
            }
            return true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Checks if tatonumber matches the format of two letters forllowed by 8 numbers.
     *
     * @param tato Tatonumber
     * @return Returns true if {@param tato} fits limitations, false if not.
     */
    private static boolean regexPass(String tato) {
        Pattern pattern = Pattern.compile("([A-z]{2})(\\d{8})");
        Matcher matcher = pattern.matcher(tato);
        return matcher.find();
    }

    /**
     * Adds new objects to the database depending on the amount of datapoints in the list.
     *
     * @param list List of objects read from import file.             check{@link CsvHandler#importCsv(String)} for more info
     * @throws SQLException the sql exception
     */
    public static void addToDBFromImportList(List<String[]> list) throws SQLException {

        for(String[] s : list){
            System.err.println(s.length);
            switch (s.length) {
                case 6, 7 -> attemptInsertIdentity(s[0], s[1], s[2], s[3], s[4], LocalDate.parse(s[5]));
                case 8, 9 -> {
                    attemptInsertIdentity(s[0], s[1], s[2], s[3], s[4], LocalDate.parse(s[5]));
                    attemptInsertWeighed(s[0], LocalDate.parse(s[7]), s[6]);
                }
                default -> attemptInsertIdentity(s[0], null, null, null, null, null);
            }
        }
    }


    /**
     * This method will query the database and retrieve all objects tagged with false on exported column.
     *
     * @return Returns a list of all objects that has not been exported.  Will return null if query fails or if no objects are found.
     */
    public static List<String[]> getItemsForExport(){
        List<String[]> result = new ArrayList<>();
        try {

            String sql = """
                SELECT identity.tatonumber, identity.rfid, identity.location, identity.race, identity.sex,
                 identity.born, w.weigh_date, w.weight
                FROM identity
                INNER JOIN weighed w on identity.tatonumber = w.tatonumber
                WHERE identity.tatonumber LIKE ? AND w.exported = false
                """;
            Connection con = LocalDatabase.connect();
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, "%%");
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                result.add(modelTable(rs).toArray());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }


    /**
     * Method will update the exported tag to true for all objects in the list.
     *
     * @param objects List of entries that has been exported to a csv file.
     * @return Returns false if the update fails, and true if no errors occurred.
     */
    public static boolean updateExportedEntry(List<String[]> objects){
        String sql = """
                    UPDATE weighed
                    SET exported = true
                    WHERE weighed.tatonumber LIKE ? AND weigh_date LIKE ?
                    """;

        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            //search from ending and upwards.
            for (String[] object  : objects) {
                pstmt.setString(1, object[0]);
                pstmt.setDate(2, Date.valueOf(object[7]));
                pstmt.executeUpdate();
            }
            return true;
            //Date.valueOf(formattedDate)
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }


    /**
     * Searches the database for objects which fits the description of the search filter and adds them into the
     * observablelist.
     *
     * @param filter Should be the values fetched from {@link NorsvinController#getSearchFilter()}
     * @param list   Adds all results into the list
     * @return Returns true if no errors occurs and false if query fails.
     */
    public static boolean search(String[] filter, ObservableList<ModelTable> list) {
        boolean result = false;

        String sql = """
                SELECT identity.*, w.weigh_date, w.weight
                FROM identity
                LEFT JOIN weighed w on identity.tatonumber = w.tatonumber
                WHERE
                (identity.tatonumber LIKE ? OR identity.rfid LIKE ?)
                AND (identity.location LIKE ?)
                AND (identity.race LIKE ?)
                AND (identity.sex LIKE ?)
                AND (w.weigh_date BETWEEN ? AND ?)
                AND (w.weight BETWEEN ? AND ?)
                AND (w.exported = ?)
                AND (identity.deceased = ?)
                """;

        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, "%" + filter[0] + "%");
            pstmt.setString(2, "%" + filter[0] + "%");
            pstmt.setString(3, "%" + filter[1] + "%");
            pstmt.setString(4, "%" + filter[2] + "%");
            pstmt.setString(5, "%" + filter[3] + "%");
            pstmt.setDate(6, Date.valueOf(filter[4]));
            pstmt.setDate(7, Date.valueOf(filter[5]));
            pstmt.setFloat(8, Float.parseFloat(filter[6]));
            pstmt.setFloat(9, Float.parseFloat(filter[7]));
            pstmt.setString(10,  filter[8] );
            pstmt.setString(11,  filter[9] );
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                list.add(modelTable(rs));
            }
            result = true;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }


    /**
     * <P>This method will connect to the local database
     * and return an object list with all objects that has not been exported.
     * It will then add the results to the list</P>
     *
     * @param table the table
     * @see ModelTable#ModelTable(String, String, int, String, String, Date, float, Date) ModelTable#ModelTable(String, String, int, String, String, Date, float, Date)ModelTable#ModelTable(String, String, int, String, String, Date, float, Date)
     */
    public static void getNotExportedObjects(ObservableList<ModelTable> table) {

        String sql = """
                SELECT identity.*, w.weigh_date, w.weight, w.exported
                FROM identity
                LEFT JOIN weighed w on identity.tatonumber = w.tatonumber
                WHERE
                (identity.tatonumber LIKE "%%" OR identity.rfid LIKE "%%")
                AND (w.exported = false OR w.weight IS NULL)
                """;
        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            //search from ending and upwards.
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                table.add(modelTable(rs));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Makes a new modelTable from the data retrieved from the database
     * @param rs    Resultset from a query.
     * @return      A new modelTable.
     */
    private static ModelTable modelTable(ResultSet rs) throws SQLException {
        return new ModelTable(
                //Sets data from database, to tableview in gui.
                rs.getString("tatonumber"),
                rs.getString("rfid"),
                rs.getInt("location"),
                rs.getString("race"),
                rs.getString("sex"),
                rs.getDate("born"),
                rs.getFloat("weight"),
                rs.getDate("weigh_date")
        );
    }

    /**
     * Makes a new modelTable from the data retrieved from the database
     * @param rs    Resultset from a query.
     * @return      A new modelTable.
     */
    private static ModelTable identityModelTable(ResultSet rs) throws SQLException {
        return new ModelTable(
                //Sets data from database, to tableview in gui.
                rs.getString("tatonumber"),
                rs.getString("rfid"),
                rs.getInt("location"),
                rs.getString("sex"),
                rs.getString("race")
        );
    }

    /**
     * Makes a new modelTable from the data retrieved from the database
     * @param rs    Resultset from a query.
     * @return      A new modelTable.
     */
    private static ModelTable weighedModelTable(ResultSet rs) throws SQLException {
        return new ModelTable(
                //Sets data from database, to tableview in gui.
                rs.getString("tatonumber"),
                rs.getFloat("weight"),
                rs.getDate("weigh_date").toLocalDate()
        );
    }

    /**
     * Makes a new modelTable from the data retrieved from the database
     * @param rs    Resultset from a query.
     * @return      A new modelTable.
     */
    private static ModelTable reducedModelTable(ResultSet rs) throws SQLException {
        return new ModelTable(
                //Sets data from database, to tableview in gui.
                rs.getString("tatonumber"),
                rs.getDate("born"),
                rs.getFloat("weight"),
                rs.getDate("weigh_date")
        );
    }


    /**
     * Attempts to add a new object into the weighed table in the database.
     *
     * @param tato   String:  This parameter will be inserted to the database as tatonumber.
     * @param date   String:  This parameter will be inserted to the database as date.
     * @param weight String:  This parameter will be inserted to the database as weight.
     * @return Returns : true if query is successful and false if it fails.
     */
    public static boolean attemptInsertWeighed(String tato, LocalDate date, String weight){
        try (Connection con = LocalDatabase.connect()) {
            String sql = """
            insert into main.weighed (tatonumber, weigh_date, weight)
            values  (?, ?, ?);
            """;

            PreparedStatement pstmt = con.prepareStatement(sql);

            //replaces the ? in the sql string at the correlated index with the parameter provided.
            pstmt.setString(1, tato);
            pstmt.setDate(2, Date.valueOf(date));
            pstmt.setFloat(3, Float.parseFloat(weight));
            pstmt.executeUpdate();
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }


    /**
     * Deletes an object from Weighed table
     *
     * @param tato Tatonumber of object that is to be deleted.
     * @param date Date of object that is to be deleted.
     * @return boolean
     */
    private static boolean deleteFromWeighed(String tato, LocalDate date){
        try (Connection con = LocalDatabase.connect()) {
            String sql = """
            DELETE FROM weighed
            WHERE tatonumber = ? AND weigh_date = ?
            """;

            PreparedStatement pstmt = con.prepareStatement(sql);
            System.err.println("Tato: " + tato + "\tDato: " + Date.valueOf(date));
            //replaces the ? in the sql string at the correlated index with the parameter provided.
            pstmt.setString(1, tato);
            pstmt.setDate(2, Date.valueOf(date));
            pstmt.executeUpdate();
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    /**
     * Deletes an object from Weighed table
     *
     * @param tato Tatonumber of object that is to be deleted.
     * @return boolean
     */
    private static boolean deleteFromIdentity(String tato){
        try (Connection con = LocalDatabase.connect()) {
            String sql = """
            DELETE FROM identity
            WHERE tatonumber = ?
            """;

            PreparedStatement pstmt = con.prepareStatement(sql);

            //replaces the ? in the sql string at the correlated index with the parameter provided.
            pstmt.setString(1, tato);
            pstmt.executeUpdate();
            return true;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return false;
    }

    /**
     * Searches the database for objects where tatonumber equals the parameter.
     *
     * @param tatonumber String used to check tatonumber and RFID.
     * @return Boolean : Returns true if the query has a match in the db, false if no matches are found.
     */
    public static List<ModelTable> getD(String tatonumber){
        String sql = """
            SELECT identity.tatonumber, identity.born, w.weight, w.weigh_date
            FROM identity
            inner join weighed w on identity.tatonumber = w.tatonumber
            WHERE identity.tatonumber = ?
            """;
        List<ModelTable> results = new ArrayList<>();
        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1,  tatonumber);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                results.add(reducedModelTable(rs));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return results;
    }

    /**
     * Deletes weighed instance, deletes the identity as well if no recorded instances of weight is present.
     *
     * @param tato Tatonumber parameter
     * @param date Date parameter
     * @return True if anything was deleted, false if not.
     */
    public static boolean deleteFromDB(String tato, LocalDate date){
        if (identityExists(tato)){
            if (hasWeight(tato)){
                deleteFromWeighed(tato, date);
                if (hasWeight(tato)){
                    return true;
                }
                else{
                    deleteFromIdentity(tato);
                    return true;
                }
            }
            else {
                deleteFromIdentity(tato);
                return true;
            }
        }
        return false;
    }

    /**
     * Edits an identity entry with new values
     *
     * @param tatonumber the tatonumber
     * @param rfid       the rfid
     * @param location   the location
     * @param sex        the sex
     * @param race       the race
     * @return Returns true if nothing fails, false if exceptions occur.
     */
    public static boolean updateIdentity(String tatonumber, String rfid, String location,
                                        String sex, String race) {
        String sql = """
                 update identity
                 set tatonumber = ?, rfid = ?, location = ?, sex = ?, race = ?
                 where tatonumber = ?
                """;

        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, tatonumber);
            pstmt.setString(2, rfid);
            pstmt.setInt(3, Integer.parseInt(location));
            pstmt.setString(4, sex);
            pstmt.setString(5, race);
            pstmt.setString(6, tatonumber);
            pstmt.executeUpdate();
            return true;
            //Date.valueOf(formattedDate)
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }


    /**
     * Check existing identity equals boolean.
     * Checks if existing entry in database equals the paramters.
     *
     * @param tatonumber the tatonumber
     * @param rfid       the rfid
     * @param location   the location
     * @param sex        the sex
     * @param race       the race
     * @return  Returns true if all datapoints are equal, false if not.
     */
    public static boolean checkExistingIdentityEquals(String tatonumber, String rfid, String location,
                                                      String sex, String race){
        String sql = """
            SELECT identity.*
            FROM identity WHERE identity.tatonumber = ?
            """;
        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            //search from ending and upwards.
            pstmt.setString(1,  tatonumber);
            ResultSet rs = pstmt.executeQuery();
            ModelTable existingData = identityModelTable(rs);
            ModelTable newData = new ModelTable(tatonumber, rfid, Integer.parseInt(location), sex, race);
            return Arrays.equals(existingData.toArray(), newData.toArray());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Check weighed equals boolean.
     * Checks if existing entry in database equals the paramters.
     *
     * @param tatonumber the tatonumber
     * @param date       the date
     * @param weight     the weight
     * @return Returns true if all datapoints are equal, false if not.
     */
    public static boolean checkWeighedEquals(String tatonumber, LocalDate date, String weight) {
        String sql = """
            SELECT weighed.*
            FROM weighed
            WHERE weighed.tatonumber = ? AND weighed.weigh_date = ?
            """;
        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            //search from ending and upwards.
            pstmt.setString(1,  tatonumber);
            pstmt.setDate(2, Date.valueOf(date));
            ResultSet rs = pstmt.executeQuery();
            ModelTable existingData = weighedModelTable(rs);
            ModelTable newData = new ModelTable(tatonumber, Float.parseFloat(weight), date);
            return Arrays.equals(existingData.toArray(), newData.toArray());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    /**
     * Edits the weight of an entry in the database
     *
     * @param tatonumber Tatonumber
     * @param date       Date of weighing
     * @param weight     Current Weight
     */
    public static void updateWeighed(String tatonumber, LocalDate date, String weight) {
        String sql = """
                 update weighed
                 set tatonumber = ?, weigh_date = ?, weight = ?
                 where tatonumber = ? AND weigh_date = ?
                """;

        try (Connection con = LocalDatabase.connect()) {
            PreparedStatement pstmt = con.prepareStatement(sql);
            pstmt.setString(1, tatonumber);
            pstmt.setDate(2, Date.valueOf(date));
            pstmt.setFloat(3, Float.parseFloat(weight));
            pstmt.setString(4, tatonumber);
            pstmt.setDate(5, Date.valueOf(date));
            pstmt.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}