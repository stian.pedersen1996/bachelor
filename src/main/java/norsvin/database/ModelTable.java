package norsvin.database;

import javafx.collections.ObservableList;
import norsvin.gui.GUIRouter;
import norsvin.gui.NorsvinController;

import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * The type Model table.
 */
public class ModelTable {

    /**
     * The Weigh date.
     */
//Table row and col.
    LocalDate weigh_date;
    /**
     * The Born.
     */
    LocalDate born;
    /**
     * The Tatonumber.
     */
    String tatonumber;
    /**
     * The Race.
     */
    String race;
    /**
     * The Sex.
     */
    String sex;
    /**
     * The Rfid.
     */
    String rfid;
    /**
     * The Location.
     */
    int location;
    /**
     * The Age.
     */
    int age;
    /**
     * The Weight.
     */
    float weight;
    /**
     * The Deceased.
     */
    boolean deceased;
    /**
     * The Exported.
     */
    boolean exported;


    /**
     * Constructor used to store the values that is returned from the SQL query.
     * {@link GUIRouter#getNotExportedItemsToSession(ObservableList)}
     *
     * @param tatonumber String: Sets local variable to be equal to parameter value.
     * @param rfid       String: Sets local variable to be equal to parameter value.
     * @param location   Int:    Sets local variable to be equal to parameter value.
     * @param race       String: Sets local variable to be equal to parameter value.
     * @param sex        String: Sets local variable to be equal to parameter value.
     * @param born       Date:   Sets local variable to be equal to parameter value.
     * @param weight     Float:  Sets local variable to be equal to parameter value.
     * @param weigh_date Date:   Sets local variable to be equal to parameter value.
     */
    public ModelTable(String tatonumber, String rfid, int location, String race, String sex, Date born, float weight, Date weigh_date) {
        this.tatonumber = tatonumber;
        this.rfid = rfid;
        this.location = location;
        this.race = race;
        this.sex = sex;
        if (born != null) {
            this.born = born.toLocalDate();
        }
        if (weigh_date != null) {
            this.weigh_date = weigh_date.toLocalDate();
        }
        this.weight = weight;

        if (this.born != null && this.weigh_date != null) {
            this.age = (int) ChronoUnit.DAYS.between(this.born, this.weigh_date);
        } else {
            if (this.born != null) {
                this.age = (int) ChronoUnit.DAYS.between(this.born, LocalDate.now());
            }
        }
    }



    /**
     * Constructor used to store the values that is returned from the SQL query.
     * {@link GUIRouter#getNotExportedItemsToSession(ObservableList)}
     *
     * @param tatonumber String: Sets local variable to be equal to parameter value.
     * @param born       Date:   Sets local variable to be equal to parameter value.
     * @param weight     Float:  Sets local variable to be equal to parameter value.
     * @param weigh_date Date:   Sets local variable to be equal to parameter value.
     */
    public ModelTable(String tatonumber, Date born, float weight, Date weigh_date) {
        this.tatonumber = tatonumber;
        if (born != null) {
            this.born = born.toLocalDate();
        }
        if (weigh_date != null) {
            this.weigh_date = weigh_date.toLocalDate();
        }
        this.weight = weight;

        if (this.born != null && this.weigh_date != null) {
            this.age = (int) ChronoUnit.DAYS.between(this.born, this.weigh_date);
        } else {
            if (this.born != null) {
                this.age = (int) ChronoUnit.DAYS.between(this.born, LocalDate.now());
            }
        }
    }

    public ModelTable(String tatonumber, String rfid, int location, String sex, String race) {
        this.tatonumber = tatonumber;
        this.rfid = rfid;
        this.location = location;
        this.race = race;
        this.sex = sex;
    }

    public ModelTable(String tatonumber, float weight, LocalDate weigh_date) {
        this.tatonumber = tatonumber;
        this.weight = weight;
        this.weigh_date = weigh_date;
    }

    /**
     * Method wil retrieve all data points in the ModelTable as a String array.
     *
     * @return String[] with all values of the ModelTable
     */
    public String[] toArray() {
        return new String[]{
                getTatonumber(),
                getRfid(),
                String.valueOf(getLocation()),
                getRace(),
                getSex(),
                String.valueOf(getBorn()),
                String.valueOf(getWeight()),
                String.valueOf(getWeigh_date()),
                String.valueOf(getAge())
        };
    }


    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current date value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public LocalDate getWeigh_date() {
        return weigh_date;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param weigh_date Sets new date value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setWeigh_date(LocalDate weigh_date) {
        this.weigh_date = weigh_date;
    }


    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current date value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public LocalDate getBorn() {
        return born;
    }


    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param born Sets new date value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setBorn(LocalDate born) {
        this.born = born;
    }


    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current tatonumber value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public String getTatonumber() {
        return tatonumber;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param tatonumber Sets new tatonumber value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setTatonumber(String tatonumber) {
        this.tatonumber = tatonumber;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current race value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public String getRace() {
        return race;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param race Sets new race value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setRace(String race) {
        this.race = race;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current sex value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public String getSex() {
        return sex;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param sex Sets new sex value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current RFID value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public String getRfid() {
        return rfid;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param rfid Sets new RFID value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setRfid(String rfid) {
        this.rfid = rfid;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current location value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public int getLocation() {
        return location;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param location Sets new location value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setLocation(int location) {
        this.location = location;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current age value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public int getAge() {
        return age;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param age Sets new age value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to retrieve data.
     *
     * @return Retrieves current weight value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public float getWeight() {
        return weight;
    }

    /**
     * Method used by {@link javafx.scene.control.cell.PropertyValueFactory} to set data.
     *
     * @param weight Sets new weight value.
     * @see NorsvinController#setSearchedObjectsToTableView() NorsvinController#setSearchedObjectsToTableView()
     */
    public void setWeight(float weight) {
        this.weight = weight;
    }


}