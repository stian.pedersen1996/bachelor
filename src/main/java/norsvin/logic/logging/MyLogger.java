package norsvin.logic.logging;

import java.util.logging.Logger;

/**
 * The type My logger.
 */
public class MyLogger {

    /**
     * Logger object, that will be used in this application
     */

    private static final Logger logg = Logger.getLogger(MyLogger.class.getPackage().toString());


    /**
     * Gets logger.
     *
     * @return the logger
     */
    public static Logger getLogger() {
        return logg;
    }
}
