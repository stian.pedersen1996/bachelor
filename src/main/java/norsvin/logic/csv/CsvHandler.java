package norsvin.logic.csv;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import norsvin.logic.logging.MyLogger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The type Csv handler.
 */
public class CsvHandler {
    /**
     * The Logg.
     */
//Gets logger object.
    Logger logg = MyLogger.getLogger();
    /**
     * The List.
     */
    List<String[]> list;


    /**
     * Export csv.
     *
     * @param fileNameAndLocation the file name and location
     * @throws Exception the exception
     */
    public void exportCsv(String fileNameAndLocation) throws Exception {
        //Instantiating the CSVWriter class

        try (CSVWriter writer = new CSVWriter(new FileWriter(fileNameAndLocation))) {
            writer.writeAll(list);
            writer.flush();


        } catch (Exception e) {
            logg.log(Level.WARNING, "Export Csv: ", e);
        }

    }

    /**
     * Add list to export.
     *
     * @param exportList the export list
     */
    public void addListToExport(List<String[]> exportList) {
        this.list = exportList;
    }

    /**
     * Gets export list.
     *
     * @return the export list
     */
    public List<String[]> getExportList() {
        return list;
    }

    /**
     * Import csv list.
     *
     * @param fileNameAndLocation the file name and location
     * @return the list
     * @throws IOException the io exception
     */
    public List<String[]> importCsv(String fileNameAndLocation) throws IOException {
        CSVReader csvReader = null;
        try (Reader reader = Files.newBufferedReader(Paths.get(fileNameAndLocation))) {
            csvReader = new CSVReader(reader);
            return csvReader.readAll();
        } catch (Exception e) {
            logg.log(Level.WARNING, "Import Csv: ", e);
        } finally {
            assert csvReader != null;
            csvReader.close();
        }

        return null;
    }
}