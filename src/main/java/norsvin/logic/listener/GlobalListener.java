package norsvin.logic.listener;

import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import norsvin.gui.NorsvinController;
import norsvin.logic.logging.MyLogger;
import norsvin.logic.scaleConnect.PropertyLoader;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The type Global listener.
 */
public class GlobalListener implements NativeKeyListener {
    /**
     * The Logg.
     */
//Gets logger object
    Logger logg = MyLogger.getLogger();
    private String key;
    private int action;
    private NorsvinController norsvinController;

    /**
     * Inherits previously initialized classes.
     *
     * @param controller     the controller
     */
    public GlobalListener(NorsvinController controller) throws IOException {
        norsvinController = controller;
        PropertyLoader propertyLoader = new PropertyLoader();
        key = propertyLoader.getPropValues("globalHotkey");
        action = Integer.parseInt(propertyLoader.getPropValues("hotkeyAction"));
    }

    public GlobalListener(){
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            logg.log(Level.WARNING, "Key logger init: ", e);
        }
        GlobalScreen.addNativeKeyListener(this);
    }

    /**
     * Needs to be here because the global keylistener requires all keypress types.
     * @param e
     */
    public void nativeKeyPressed(NativeKeyEvent e) {}

    /**
     * Simulates a "CTRL + V" keypress.
     */
    private void pasteClipboard() {
        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            robot.keyPress(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_V);
            robot.keyRelease(KeyEvent.VK_CONTROL);
        } catch (AWTException e) {
            logg.log(Level.WARNING, "Paste to clipboard:  ", e);
        }
    }

    /**
     * Simulates a "TAB" keypress.
     */
    private void tab() {
        try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_TAB);
            robot.keyRelease(KeyEvent.VK_TAB);
        } catch (AWTException e) {
            logg.log(Level.WARNING, "Tab press:  ", e);
        }
    }


    private void configuredOperandi() throws IOException, InterruptedException {
        switch (action) {
            case 1 -> {
                //Only weight
                setToClipboard(norsvinController.guiRouter.silentWeigh(norsvinController.propertyLoader));
                pasteClipboard();
            }
            case 2 -> {
                //Only date
                setToClipboard(LocalDateTime.now().toString());
                pasteClipboard();
            }
            case 3 -> {
                //weight and date
                setToClipboard(norsvinController.guiRouter.silentWeigh(norsvinController.propertyLoader));
                pasteClipboard();
                wait(100);
                tab();
                wait(100);
                setToClipboard(LocalDateTime.now().toString());
                pasteClipboard();
            }
        }
    }

    /**
     * Copies the parameter into clipboard.
     *
     * @param weight String
     */
    private void setToClipboard(String weight) {
        StringSelection selection = new StringSelection(weight);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
    }

    /**
     * Will listen for keypresses globally on the computer and execute code when the specified key is pressed.
     * @param e
     */
    public void nativeKeyReleased(NativeKeyEvent e) {
        try {
            if (NativeKeyEvent.getKeyText(e.getKeyCode()).equals(key)) {
                try {
                    configuredOperandi();
                } catch (Exception nativeHookException) {
                    logg.log(Level.WARNING, "Native key pressed:  ", nativeHookException);
                }
            }
        } catch (Exception ioException) {
            logg.log(Level.WARNING, "IOException:  ", ioException);
        }
    }

    /**
     * Needs to be here because the global keylistener requires all keypress types.
     * @param e
     */
    public void nativeKeyTyped(NativeKeyEvent e) {
    }
}
