package norsvin.logic.scaleConnect;


import norsvin.logic.logging.MyLogger;

import javax.bluetooth.UUID;
import javax.bluetooth.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Services search.
 */
public class ServicesSearch {
    /**
     * The Logg.
     */
//Gets logger object
    Logger logg = MyLogger.getLogger();


    /* To find serial port services */
    private final UUID SERIAL_PORT = new UUID(0x1101);
    /* Get URL attribute from bluetooth service */
    private final int URL_ATTRIBUTE = 0X0100;

    /**
     * Gets bluetooth devices.
     *
     * @return the bluetooth devices
     */
    public Map<String, List<String>> getBluetoothDevices() {
        /**
         * Find service on bluetooth device
         */
        /* Initialize UUID Array */
        UUID[] searchUuidSet = new UUID[]{SERIAL_PORT};
        final Object serviceSearchCompletedEvent = new Object();
        int[] attrIDs = new int[]{URL_ATTRIBUTE};

        /* Create an object to get list of devices in range or paired */
        RemoteDeviceDiscovery remoteDeviceDiscovery = new RemoteDeviceDiscovery();
        /* Creates  a hash map that returns Bluetooth device address, name and URL */
        final Map<String, List<String>> hashMapResult = new HashMap<>();

        try {
            /* Create an object of DiscoveryListener */
            DiscoveryListener listener = new DiscoveryListener() {

                /* Never used, but has to be here because of DiscoveryListener */
                public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
                }

                /* Never used, but has to be here because of DiscoveryListener */
                public void inquiryCompleted(int discType) {
                }

                /* Find service URL of bluetooth device */
                public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
                    for (int i = 0; i < servRecord.length; i++) {
                        DataElement serviceName = servRecord[i].getAttributeValue(URL_ATTRIBUTE);

                        if (serviceName.getValue().toString().contains("SPP")) {
                            /* Find URL of bluetooth device */
                            String url = servRecord[i].getConnectionURL(ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
                            if (url == null) {
                                continue;
                            }
                            /* Get object of bluetooth device */
                            RemoteDevice rd = servRecord[i].getHostDevice();

                            /* Put it in a map */
                            hashMapResult.get(rd.getBluetoothAddress()).add(url);

                        }
                    }
                }


                public void serviceSearchCompleted(int transID, int respCode) {
                    /* Notify thread when search completed */
                    synchronized (serviceSearchCompletedEvent) {
                        serviceSearchCompletedEvent.notifyAll();
                    }
                }
            };

            /* Get list of bluetooth device from class RemoteDeviceDiscovery */
            for (Enumeration en = remoteDeviceDiscovery.getDevices().elements(); en.hasMoreElements(); ) {
                /* Get RemoteDevice object */
                RemoteDevice btDevice = (RemoteDevice) en.nextElement();
                /* Create list to return details */
                List<String> listDeviceDetails = new ArrayList<String>();

                try {
                    /* Add bluetooth device name and address in list */
                    listDeviceDetails.add(btDevice.getFriendlyName(false));
                    listDeviceDetails.add(btDevice.getBluetoothAddress());

                } catch (Exception e) {
                    logg.log(Level.WARNING, "Exception adding device and adress to list: ", e);
                }

                /* Put bluetooth device details in hash map */
                hashMapResult.put(btDevice.getBluetoothAddress(), listDeviceDetails);
                synchronized (serviceSearchCompletedEvent) {
                    LocalDevice.getLocalDevice().getDiscoveryAgent().searchServices(attrIDs, searchUuidSet, btDevice, listener);
                    serviceSearchCompletedEvent.wait();

                }
            }
        } catch (Exception e) {
            logg.log(Level.WARNING, "Searching for new devices:   ", e);


        }
        /* Return a hash map of result. */
        return hashMapResult;
    }

}