package norsvin.logic.scaleConnect;


import norsvin.logic.logging.MyLogger;

import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The type Bluetooth controller.
 */
public class BluetoothController {
    /**
     * The Logg.
     */
//Gets logger object
    Logger logg = MyLogger.getLogger();
    /**
     * The Url.
     */
    String url;
    /**
     * The Stream connection.
     */
    StreamConnection streamConnection = null;
    /**
     * The Out stream.
     */
    OutputStream outStream = null;
    /**
     * The P writer.
     */
    PrintWriter pWriter = null;
    /**
     * The In stream.
     */
    InputStream inStream = null;
    /**
     * The Buffer reader.
     */
    BufferedReader bReader = null;

    /**
     * Makes and Outstream and InputStream. Return respons from scale to readCRLFLine
     *
     * @param serverURL the server url
     * @return the string
     * @throws InterruptedException the interrupted exception
     */
    public String requestWeight(String serverURL) throws InterruptedException {

        try {

            streamConnection = (StreamConnection) Connector.open(serverURL);
            outStream = streamConnection.openOutputStream();
            //read response
            inStream = streamConnection.openInputStream();

            url = serverURL;


            pWriter = new PrintWriter(new OutputStreamWriter(outStream));
            //Sends command to scale to return weight.
            pWriter.write(005);
            pWriter.flush();


            bReader = new BufferedReader(new InputStreamReader(inStream));

            //Closes outStream and connection.
            outStream.flush();
            outStream.close();
            streamConnection.close();


        } catch (IOException e) {

            logg.log(Level.WARNING, "Requesting weight:  ", e);
        }

        return readCRLFLine(bReader);

    }


    /**
     * Read result from inputStream / bufferreader. And closes stream afterwards.
     *
     * @param bReader
     * @return -1 if there is any problem with connection / result.
     * @return weight result.
     */

    private String readCRLFLine(BufferedReader bReader) {


        //Result from the scale be appened to result.
        StringBuilder result = new StringBuilder();

        //The functin will have to sleep for 1 sec, to get all the results from the bufferReader.
        try {
            Thread.sleep(500);

            //If scale is out of reach, or bluetooth not working. It will return -2.
            if (bReader == null)
                return "-2";


            while (true) {
                //Checks if the next readline is empty. If not it will append result.
                if (!bReader.ready()) break;

                String next = null;

                //Appends result before \r and \n
                next = bReader.readLine().trim();
                result.append(next);
            }


            //If scale dosent respond, readCRFLINE will return -1.
            String temp = result.toString();
            if (temp.equals(""))
                return "-1";


        } catch (Exception e) {
            logg.log(Level.WARNING, "Reading from buffer reader:  ", e);
        } finally {
            // this block will be executed in every case, success or caught exception
            if (bReader != null) {
                // again, a resource is involved, so try-catch another time
                try {
                    //This will close the bufferReader, and the inputStream to the scale.
                    bReader.close();
                    inStream.close();
                } catch (IOException e) {
                    logg.log(Level.WARNING, "Reading and closing buffer reader:   ", e);
                }
            }
        }
        //Returns the weight in kg.
        return result.substring(2, result.length() - 7);
    }
}