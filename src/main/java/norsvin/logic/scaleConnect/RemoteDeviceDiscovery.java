package norsvin.logic.scaleConnect;


import norsvin.logic.logging.MyLogger;

import javax.bluetooth.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Remote device discovery.
 */
public class RemoteDeviceDiscovery {
    /**
     * The Logg.
     */
//Gets logger object
    Logger logg = MyLogger.getLogger();

    /**
     * Gets devices.
     *
     * @return the devices
     */
    public Vector getDevices() {
        /* Create Vector variable */
        final Vector devicesDiscovered = new Vector();
        try {
            final Object inquiryCompletedEvent = new Object();
            /* Clear Vector variable */
            devicesDiscovered.clear();

            /* Create an object of DiscoveryListener */
            DiscoveryListener listener = new DiscoveryListener() {

                public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
                    /* Get devices paired with system or in range(Without Pair) */
                    devicesDiscovered.addElement(btDevice);
                }

                public void inquiryCompleted(int discType) {
                    /* Notify thread when inquiry completed */
                    synchronized (inquiryCompletedEvent) {
                        inquiryCompletedEvent.notifyAll();
                    }
                }

                /* Never used, but has to be here because of DiscoveryListener */
                public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
                }

                /* Never used, but has to be here becuase of DiscoveryListener*/
                public void serviceSearchCompleted(int transID, int respCode) {
                }


            };

            synchronized (inquiryCompletedEvent) {
                /* Starts discovery of devices */
                boolean started = LocalDevice.getLocalDevice().getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC, listener);

                if (started) {

                    inquiryCompletedEvent.wait();
                }
            }
        } catch (Exception e) {
            logg.log(Level.WARNING, "Remote devices, check if bluetooth is turned on ", e);
        }

        /* Returns list of found devices */
        return devicesDiscovered;
    }
}