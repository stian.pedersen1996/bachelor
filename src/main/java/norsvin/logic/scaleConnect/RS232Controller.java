package norsvin.logic.scaleConnect;

import com.fazecast.jSerialComm.SerialPort;
import norsvin.logic.logging.MyLogger;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


//package com.fazecast.jSerialComm

/**
 * The type Rs 232 controller.
 */
public class RS232Controller {
    /**
     * The Sp.
     */
    static SerialPort sp;
    /**
     * The Serials.
     */
    static SerialPort[] serials;
    private static final ArrayList<SerialPort> test = new ArrayList();
    /**
     * The Logg.
     */
//Gets logger object
    Logger logg = MyLogger.getLogger();

    /**
     * Connection array list.
     *
     * @return the array list
     */
    public static ArrayList<SerialPort> connection() {

        serials = SerialPort.getCommPorts();
        for (SerialPort serial : serials) {
            test.add(serial);
            System.out.println("get descPortName: " + serial.getDescriptivePortName());
            System.out.println("getPortDesc: " + serial.getPortDescription());
            System.out.println("getSystemPortName: " + serial.getSystemPortName());
        }

        //Returns Arraylist of objects.
        return test;
    }


    /**
     * Connect to device.
     */
    public void connectToDevice() throws IOException {
        PropertyLoader propertyLoader = new PropertyLoader();

        sp = SerialPort.getCommPort(propertyLoader.getPropValues("previouslySelectedPort"));
        sp.setComPortParameters(9600, 7, 1, 2); // settings given from DIGI
        sp.setComPortTimeouts(SerialPort.TIMEOUT_WRITE_BLOCKING, 0, 0); // block until bytes can be written

    }


    /**
     * Request weight string.
     *
     * @return the string
     */
    public String requestWeight() {
        String lineRead = null;
        try {
            connectToDevice();
            OutputStream outStream = sp.getOutputStream();
            PrintWriter pWriter = new PrintWriter(new OutputStreamWriter(outStream));
            outStream.write(005);
            Thread.sleep(2000);
            pWriter.flush();
            System.out.println("Sent command \n Waiting for response");


            InputStream inStream = sp.getInputStream();
            BufferedReader bReader2 = new BufferedReader(new InputStreamReader(inStream));
            Thread.sleep(2000);


            lineRead = bReader2.readLine();

            System.out.println("Line read: " + lineRead);


            outStream.flush();
            outStream.close();
            pWriter.close();
            bReader2.close();


        } catch (Exception e) {
            logg.log(Level.WARNING, "Reading weight RS232:  ", e);
        }
        return lineRead;
    }
}
