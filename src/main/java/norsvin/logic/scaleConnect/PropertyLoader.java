package norsvin.logic.scaleConnect;

import norsvin.logic.logging.MyLogger;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Property loader.
 */
public class PropertyLoader{
    /**
     * The Logg.
     */
//Gets logger object
    private Logger logg = MyLogger.getLogger();
    /**
     * The Result.
     */
    private String result = "";
    /**
     * The Input stream.
     */
    private InputStream inputStream;

    /**
     * Gets prop values.
     *
     * @param property the property
     * @return the prop values
     * @throws IOException the io exception
     */
    public String getPropValues(String property) throws IOException {

        try {
            Properties prop = new Properties();
            String propFileName = "config/launch.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }


            // get the property value and print it out

            //Todo make this as a hashmap, so this can be used for rs232 also.
            result = prop.getProperty(property);

        } catch (Exception e) {
            logg.log(Level.WARNING, "Get property values: ", e);
        } finally {
            assert inputStream != null;
            inputStream.close();
        }
        return result;
    }

    /**
     * Sets prop value.
     *
     * @param property the property
     * @param newValue the new value
     * @throws IOException the io exception
     */
//Set value to property value.
    public void setPropValue(String property, String newValue) throws IOException {


        Properties props = null;
        try (FileInputStream in = new FileInputStream("src/main/resources/config/launch.properties") ) {

            props = new Properties();
            props.load(in);

        } catch (Exception e) {
            logg.log(Level.WARNING, "Input stream prop value:  ", e);
        }

        try (FileOutputStream out = new FileOutputStream("src/main/resources/config/launch.properties")) {

            assert props != null;
            props.setProperty(property, newValue);
            props.store(out, null);
        } catch (Exception e) {
            logg.log(Level.WARNING, "Outstream prop value:  ", e);
        }
        finally {
            assert props != null;
            props.clear();
        }
    }
}
