package norsvin.gui;

import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import norsvin.database.LocalDatabase;
import norsvin.database.ModelTable;
import norsvin.exceptions.alerts.AlertMessage;
import norsvin.logic.csv.CsvHandler;
import norsvin.logic.logging.MyLogger;
import norsvin.logic.scaleConnect.BluetoothController;
import norsvin.logic.scaleConnect.PropertyLoader;
import norsvin.logic.scaleConnect.RS232Controller;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Gui controller.
 */
public class GUIRouter {
    /**
     * The Logg.
     */
// Gets logger for exceptions
    Logger logg = MyLogger.getLogger();


    private static final String BASE_LOCATION = "i18n.Language";
    /**
     * Searches the database with search filter and adds results to search table.
     *
     * @param list   Search table list
     * @param filter String array with filter values.
     */
    public void searchDBWithFilter(ObservableList<ModelTable> list, String[] filter) {
        LocalDatabase.search(filter, list);
    }


    /**
     * Method will forst check if data already exists in the database.
     * data other than primary key is is altered, it will then update old data into new
     * lastly, if primary key is not found, it will add parameter data into the database.
     *
     * @param tatonumber the tatonumber
     * @param rfid       the rfid
     * @param location   the location
     * @param sex        the sex
     * @param race       the race
     * @param date       the date
     * @param weight     the weight
     * @throws SQLException the sql exception
     * @see LocalDatabase#attemptInsertIdentity(String, String, String, String, String, LocalDate)
     * @see LocalDatabase#attemptInsertWeighed(String, LocalDate, String)
     */
    public void addToDB(String tatonumber, String rfid, String location, String sex, String race,
                        LocalDate date, String weight) throws SQLException {

        if (LocalDatabase.identityExists(tatonumber)){
            if (!LocalDatabase.checkExistingIdentityEquals(tatonumber, rfid, location, sex, race)){
                LocalDatabase.updateIdentity(tatonumber, rfid, location, sex, race);
            }
        } else {
            LocalDatabase.attemptInsertIdentity(tatonumber, rfid, location, sex, race, date);
        }
        if (LocalDatabase.weighedExists(tatonumber, date)){
            if (!LocalDatabase.checkWeighedEquals(tatonumber, date, weight)){
                LocalDatabase.updateWeighed(tatonumber, date, weight);
            }
        } else{
            LocalDatabase.attemptInsertWeighed(tatonumber, date, weight);
        }


    }

    /**
     * Deletes an object in the database that has equal tatonumber and date as the parameter.
     *
     * @param tato String of the tatonumber.
     * @param date String of date
     */
    public void deleteFromDB(String tato, LocalDate date){
        //Displays confirmation alert.
        if (AlertMessage.confirmDelete("norsvin.alert.title.confirmation",
                "norsvin.alert.header.confirmation",
                "norsvin.alert.context.confirmation", tato)){
            if(!LocalDatabase.deleteFromDB(tato, date)){
                //Displays error message, when deletion was not successful.
                AlertMessage.showAlert("norsvin.alert.title.error","norsvin.alert.header.confirmation",
                        "norsvin.alert.context.error", tato);
            }

        }
    }


    /**
     * Method used for finding data in the {@link TableView<ModelTable>} and returns them in a
     * <code>String</code> array.
     * Will return empty strings if an invalid index is provided
     *
     * @param table The table of content
     * @return Indexes : 0: TatoNumber 1: RFID 2: Weight 3: Date 4: Age
     *   days between birth date and current system date 5: Location 6: Sex 7: Race 8: Current system date
     */
    public String[] getSelectedRow(TableView<ModelTable> table) {

        int index = table.getSelectionModel().getSelectedIndex();
        //Non existent index - fast return
        if (index <= -1) {
            return new String[]{"", "", "", "", "", "", "", "", "", ""};
        }
        return new String[]{
                String.valueOf(table.getItems().get(index).getTatonumber()),
                String.valueOf(table.getItems().get(index).getRfid()),
                String.valueOf(table.getItems().get(index).getWeight()),
                String.valueOf(table.getItems().get(index).getWeigh_date()),
                String.valueOf(ChronoUnit.DAYS.between(table.getItems().get(index).getBorn(), LocalDate.now())),
                String.valueOf(table.getItems().get(index).getLocation()),
                String.valueOf(table.getItems().get(index).getSex()),
                String.valueOf(table.getItems().get(index).getRace()),
                LocalDate.now().toString()
        };
    }


    /**
     * Method is implemented because the standard format of DatePicker is "MM-dd-yyyy"
     * This method changes default format to "yyyy-MM-dd"
     *
     * @param datePicker The FXML DatePicker object
     */
    public void setDateFormat(DatePicker datePicker) {
        datePicker.setConverter(new StringConverter<>() {
            final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            @Override
            public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }
            @Override
            public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }


    /**
     * <P>This method will connect to the local database
     * and return an object list with all objects that not exported.</P>
     *
     * @param oblist The list should be a ModelTable.
     * @see ModelTable#ModelTable(String, String, int, String, String, Date, float, Date)
     */
    public void getNotExportedItemsToSession(ObservableList<ModelTable> oblist) {
        LocalDatabase.getNotExportedObjects(oblist);
    }


    /**
     * Exports all unexported objects in the database to a .csv file and tags them as exported.
     *
     * @param rootContainer The controller for the main application.
     * @return Returns boolean values if the export fails or not.
     */
    public boolean exportSession(VBox rootContainer) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("CSV Files(*.csv)", "*.csv"));
        File selectedFile = fileChooser.showSaveDialog(rootContainer.getScene().getWindow());
        if (selectedFile != null){
            if (!selectedFile.getName().endsWith(".csv")){
                selectedFile = new File(selectedFile.getAbsolutePath() + ".csv");
            }
            CsvHandler exporter = new CsvHandler();
            try {
                exporter.addListToExport(LocalDatabase.getItemsForExport());
                if (exporter.getExportList() == null) {
                    selectedFile.delete();
                    return false;}
                exporter.exportCsv(selectedFile.getAbsolutePath());
                return LocalDatabase.updateExportedEntry(exporter.getExportList());
            } catch (Exception e) {
                logg.log(Level.WARNING,"Export session", e);
            }
        }
        return false;
    }


    /**
     * Imports objects from a .csv file and adds them to the database.
     *
     * @param rootContainer The controller for the main application.
     * @return Returns boolean values if the import fails or not.
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    public boolean importSession(VBox rootContainer) throws IOException, SQLException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setSelectedExtensionFilter(new FileChooser.ExtensionFilter("CSV Files(*.csv)", "*.csv"));
        File selectedFile = fileChooser.showOpenDialog(rootContainer.getScene().getWindow());
        if (!selectedFile.getName().endsWith(".csv")) {
            return false;
        }
        CsvHandler importer = new CsvHandler();
        LocalDatabase.addToDBFromImportList(importer.importCsv(selectedFile.toString()));

        return true;
    }

    /**
     * Weigh string.
     * @return the string
     * @throws IOException          the io exception
     * @throws InterruptedException the interrupted exception
     */
    public String weigh(PropertyLoader propertyLoader) throws IOException, InterruptedException {
        //Check properti file for default weight scale.
        String defaultScale = propertyLoader.getPropValues("defaultScale");
        RS232Controller rs232Controller = new RS232Controller();
        switch(defaultScale) {
            case "Bluetooth Scale":
                return getBluetoothWeight(propertyLoader);
            case "RS232 Scale":
                return rs232Controller.requestWeight();
            case "":
                connectionAlert(propertyLoader);
                break;
            default:
        }
        return "";
    }

    /**
     * Weigh string.
     * @return the string
     * @throws IOException          the io exception
     * @throws InterruptedException the interrupted exception
     */
    public String silentWeigh(PropertyLoader propertyLoader) throws IOException, InterruptedException {
        //Check properti file for default weight scale.
        String defaultScale = propertyLoader.getPropValues("defaultScale");
        RS232Controller rs232Controller = new RS232Controller();
        switch(defaultScale) {
            case "Bluetooth Scale":
                return getSimpleBluetoothWeight(propertyLoader);
            case "RS232 Scale":
                return rs232Controller.requestWeight();
            case "":
                return ("No scale selected - No scale is selected for use  - Go to Settings > Bluetooth/RS232");
            default:
        }
        return "";
    }


    /**
     * Connection alert.
     *
     * @param propertyLoader the property loader
     */
    public void connectionAlert(PropertyLoader propertyLoader){
        //Language bundle, i18n. Finds users pref. language
        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());

        Alert alert= AlertMessage.selectScale("norsvin.bluetooth.alert.title.scaleSelected",
                "norsvin.bluetooth.alert.header.scaleSelected",
                "norsvin.bluetooth.alert.context.scaleSelected");

        String bluetoothText = bundle.getString("norsvin.scale.bluetooth");
        String rs232Text = bundle.getString("norsvin.scale.rs232");
        String cancelText = bundle.getString("norsvin.cancel");

        ButtonType okButton = new ButtonType(bluetoothText, ButtonBar.ButtonData.YES);
        ButtonType noButton = new ButtonType(rs232Text, ButtonBar.ButtonData.NO);
        ButtonType cancelButton = new ButtonType(cancelText, ButtonBar.ButtonData.CANCEL_CLOSE);
        alert.getButtonTypes().setAll(okButton, noButton, cancelButton);
        //Checks if user had set prefered scale for weighing.
        alert.showAndWait().ifPresent(type -> {
            try {
                if (type == okButton) {
                    propertyLoader.setPropValue("defaultScale", "Bluetooth Scale");
                    //comboBoxScale.setValue(optionScale.get(0));
                    createBluetoothDeviceScene();
                } else if (type == noButton) {
                    propertyLoader.setPropValue("defaultScale", "RS232 Scale");
                    //Sets value to combobox to show selected/used scale.
                    //comboBoxScale.setValue(optionScale.get(1));
                    createRS232DeviceScene();
                }
            } catch (IOException e) {
                logg.log(Level.WARNING,"Connection alert", e);
            }
        });
    }


    /**
     * Gets weight from bluetooth device and updates weight text field.
     *
     * @param propertyLoader        the property loader
     * @return the bluetooth weight
     * @throws IOException          the io exception
     * @throws InterruptedException the interrupted exception
     */
    public String getBluetoothWeight(PropertyLoader propertyLoader)
            throws IOException, InterruptedException {
        String result;
        //Check if user had already configurated bluetooth scale.
        String url = propertyLoader.getPropValues("previouslySelectedScale");

        BluetoothController bluetoothController = new BluetoothController();
        if(url.equals("")){
            result = "-3";
        }else{
            result = bluetoothController.requestWeight(url);
        }

        switch(result) {
            case "-1":

                AlertMessage.showAlert(
                        "norsvin.bluetooth.title.scaleOFF",
                        "norsvin.bluetooth.header.scaleOFF",
                        "norsvin.bluetooth.context.scaleOFF");
                break;

            case "-2":
                AlertMessage.showAlert(
                        "norsvin.bluetooth.title.outofreach",
                        "norsvin.bluetooth.header.outofreach",
                        "norsvin.bluetooth.context.outofreach");
                break;
            case "-3":

                AlertMessage.showAlert(
                         "norsvin.device.title.configurated",
                        "norsvin.device.header.configurated",
                        "norsvin.device.context.configurated");
                break;
            default:
        }
        return result;
    }


    /**
     * Gets weight from bluetooth device and updates weight text field.
     *
     * @param propertyLoader Property file containing configured options.
     * @return Returns either weight returned from scale, or an error code.
     * @throws IOException          the io exception
     * @throws InterruptedException the interrupted exception
     */
    public String getSimpleBluetoothWeight(PropertyLoader propertyLoader) throws IOException, InterruptedException {
        String result;
        //Check if user had already configurated bluetooth scale.
        String url = propertyLoader.getPropValues("previouslySelectedScale");

        BluetoothController bluetoothController = new BluetoothController();
        if (url.equals("")) {
            result = "-3";
        } else {
                result = bluetoothController.requestWeight(url);
        }
        switch(result) {
            case "-1":
                return ("Bluetooth error - No weight received! - Check if scale is turned on");
            case "-2":
                return ("Bluetooth error - Can't reach scale! - Check if scale is in reach");
            case "-3":
                return ("No bluetooth device or RS232 is configured! - Go to Settings > Bluetooth/RS232'");
            default:
        }
        return result;
    }


    /**
     * Create rs 232 device scene.
     *
     * @throws IOException the io exception
     */
    public void createRS232DeviceScene() throws IOException {

        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());
        //Load scene with selected language.
        // The loader.
        //FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/RS232Port" + ".fxml"), bundle);
        //Parent root = new FXMLLoader(getClass().getResource("/gui/RS232Port" + ".fxml"), bundle).load();
        Scene scene = new Scene(new FXMLLoader(
                getClass().getResource("/gui/RS232Port" + ".fxml"), bundle).load());
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.getIcons().add(new Image("img/N_icon_logo.png"));
        stage.setTitle(bundle.getString("norsvin.title"));
        stage.show();
    }


    /**
     * Creates bluetooth device scene, and sets controller.
     *
     * @throws IOException the io exception
     */
    public void createBluetoothDeviceScene() throws IOException {

        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());
        //Load scene with selected language.
        // The loader.
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/BluetoothDevices" + ".fxml"), bundle);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.getIcons().add(new Image("img/N_icon_logo.png"));
        stage.setTitle(bundle.getString("norsvin.title"));
        stage.show();
    }


    /**
     * Update graph.
     * @param xAxis       the x axis
     * @param yAxis       the y axis
     * @param lineGraph   the line graph
     * @param searchTable the search table
     *
     *
     */
    public void addToGraph(NumberAxis xAxis, NumberAxis yAxis, LineChart<Number, Number> lineGraph,
                           TableView<ModelTable> searchTable) {
        xAxis.setLabel("Days from born");
        yAxis.setLabel("Weight");
        lineGraph.setTitle("Pig");
        ObservableList<ModelTable> temp1 = searchTable.getItems();

        List<String> tatoList = new ArrayList<>();
        for (ModelTable s : temp1) {
            tatoList.add(s.getTatonumber());
            //if (tatoSelected.equals(s.getTatonumber()))
            //dataSeries1.getData().add(new XYChart.Data(s.getAge(), s.getWeight()));
        }
        Set<String> utl = new HashSet<>(tatoList);
        System.out.println(Arrays.toString(utl.toArray()));
        for (String tmp: utl) {
            XYChart.Series dataSeries1 = new XYChart.Series();
            List<ModelTable> queryResultater =  LocalDatabase.getD(tmp);
            for (ModelTable res : queryResultater){
                System.out.println(Arrays.toString(res.toArray()));
                dataSeries1.getData().add(new XYChart.Data(res.getAge(), res.getWeight()));
            }
            dataSeries1.setName(tmp);
            lineGraph.getData().add(dataSeries1);

        }
    }

    public void openPreferences(VBox rootContainer) throws IOException {
        ResourceBundle bundle = ResourceBundle.getBundle(BASE_LOCATION, Locale.getDefault());
        //Load scene with selected language.
        // The loader.
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/Config" + ".fxml"), bundle);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        //Set controller class.
        PreferencesController controller = loader.getController();
        controller.setRootContainer(rootContainer);

        Stage stage = new Stage();
        stage.setScene(scene);
        stage.getIcons().add(new Image("img/N_icon_logo.png"));
        stage.setTitle(bundle.getString("norsvin.title"));
        stage.show();
    }
}