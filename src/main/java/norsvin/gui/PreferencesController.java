package norsvin.gui;

/**
 * Sample Skeleton for 'Config.fxml' Controller Class
 */

import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import norsvin.exceptions.alerts.AlertMessage;
import norsvin.logic.logging.MyLogger;
import norsvin.logic.scaleConnect.PropertyLoader;


/**
 * The type Preferences controller.
 */
public class PreferencesController {

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="languageComboBox"
    private ComboBox<String> languageComboBox; // Value injected by FXMLLoader

    @FXML // fx:id="hotkeyButton"
    private Button hotkeyButton; // Value injected by FXMLLoader

    @FXML // fx:id="scaleSelectComboBox"
    private ComboBox<String> scaleSelectComboBox; // Value injected by FXMLLoader

    @FXML // fx:id="dateCheckBox"
    private CheckBox dateCheckBox; // Value injected by FXMLLoader

    @FXML // fx:id="weightCheckBox"
    private CheckBox weightCheckBox; // Value injected by FXMLLoader

    @FXML // fx:id="applyButton"
    private Button applyButton; // Value injected by FXMLLoader

    private VBox rootContainer;

    /**
     * The Option scale.
     */
    private final ObservableList<String> optionScale =
            FXCollections.observableArrayList("Bluetooth Scale", "RS232 Scale");

    /**
     * The Language status list.
     */
    ObservableList<String> languageStatusList =
            FXCollections.observableArrayList("English", "Norwegian", "Netherlands");


    private final String[] languagecode = {"en-US", "nb-NO", "nl-NL"};

    private final Logger logg = MyLogger.getLogger();

    /**
     * Set root container.
     *
     * @param rootContainer the root container
     */
    public void setRootContainer(VBox rootContainer){
        this.rootContainer = rootContainer;
    }

    /**
     * Stores all changes done in the config scene.
     * These changes are stored in the launch.properties file
     *
     * @param event the event
     * @throws Exception the exception
     */
    @FXML
    void applyChanges(ActionEvent event) throws Exception {

        int temp = 0;
        try {
            PropertyLoader propertyLoader = new PropertyLoader();
            propertyLoader.setPropValue("globalHotkey", hotkeyButton.getText());
            if (dateCheckBox.isSelected()){temp += 2;}
            if (weightCheckBox.isSelected()){temp += 1;}
            propertyLoader.setPropValue("hotkeyAction", String.valueOf(temp));
            propertyLoader.setPropValue("previouslySelectedLanguage",
                    languagecode[languageComboBox.getSelectionModel().getSelectedIndex()]);
            propertyLoader.setPropValue("defaultScale",
                    optionScale.get(scaleSelectComboBox.getSelectionModel().getSelectedIndex()));
            Locale.setDefault(
                    Locale.forLanguageTag(languagecode[languageComboBox.getSelectionModel().getSelectedIndex()]));
            reload(propertyLoader);
            // get a handle to the stage
            //Stage stage = (Stage) applyButton.getScene().getWindow();
            // do what you have to do
            //stage.close();

            //Button fx:id="applyButton"
        } catch (IOException e) {
            logg.log(Level.WARNING,"Setting properties:  ", e);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Legg inn verdi fra hotkkeyButton
    }

    /**
     * Reloads the main view to new selected language
     * @param propertyLoader
     * @throws IOException
     */
    private void reload(PropertyLoader propertyLoader) throws IOException {
        ResourceBundle bundle = ResourceBundle.getBundle("i18n.Language",
                Locale.forLanguageTag(languagecode[languageComboBox.getSelectionModel().getSelectedIndex()]));
        //Load scene with selected language.
        Parent root = FXMLLoader.load(
                Objects.requireNonNull(getClass().getResource("/gui/Main.fxml")), bundle);
        Stage window = (Stage) rootContainer.getScene().getWindow();
        window.setScene(new Scene(root));
    }

    /**
     * Filtered keys that should not be able to be used as hotkey because they have inbuild functions in webbrowsers
     * @param e
     * @return
     */
    private boolean keyFilter(KeyEvent e){
        return switch (e.getCode().toString()) {
            case "F1", "F3", "F5", "F6", "F7", "F10", "F11", "F12" -> false;
            default -> true;
        };
    }

    /**
     * Initialize.
     */
    @FXML
    // This method is called by the FXMLLoader when initialization is complete
    public void initialize() {
        assert languageComboBox != null : "fx:id=\"languageComboBox\" " +
                "was not injected: check your FXML file 'Config.fxml'.";
        assert hotkeyButton != null : "fx:id=\"hotkeyButton\"" +
                " was not injected: check your FXML file 'Config.fxml'.";
        assert scaleSelectComboBox != null : "fx:id=\"scaleSelectComboBox\" " +
                "was not injected: check your FXML file 'Config.fxml'.";
        assert dateCheckBox != null : "fx:id=\"dateCheckBox\" " +
                "was not injected: check your FXML file 'Config.fxml'.";
        assert weightCheckBox != null : "fx:id=\"weightCheckBox\"" +
                " was not injected: check your FXML file 'Config.fxml'.";
        assert applyButton != null : "fx:id=\"applyButton\" " +
                "was not injected: check your FXML file 'Config.fxml'.";
        //TODO: date option does not print properly - and is thus disabled.
        dateCheckBox.setDisable(true);
        scaleSelectComboBox.setItems(optionScale);
        languageComboBox.setItems(languageStatusList);


        try  {
            PropertyLoader propertyLoader = new PropertyLoader();
            hotkeyButton.setText(propertyLoader.getPropValues("globalHotkey"));
            int temp = Integer.parseInt(propertyLoader.getPropValues("hotkeyAction"));

            scaleSelectComboBox.setValue(propertyLoader.getPropValues("defaultScale"));
            languageComboBox.setValue(languageStatusList.get(checkLanguage(propertyLoader)));
            switch (temp){
                case 1:
                    weightCheckBox.setSelected(true);
                    System.out.println("Case 1");
                    break;
                case 2:
                    dateCheckBox.setSelected(true);
                    System.out.println("Case 2");
                    break;
                case 3:
                    System.out.println("Case 3");
                    weightCheckBox.setSelected(true);
                    dateCheckBox.setSelected(true);
                    break;
                default:
            }

        } catch (IOException e) {
            logg.log(Level.WARNING,"Fetching properties:  ", e);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Look for new hotkey.
     * then removes focus from the button to prevent further edits
     *
     * @param keyEvent the key event
     */
    public void lookForNewKey(KeyEvent keyEvent) {
        if (!keyFilter(keyEvent)){
            AlertMessage.showAlert("norsvin.config.alerts.keyTitle",
                    "norsvin.config.alerts.keyHeader", "norsvin.config.alerts.keyContent");
            return;}
        hotkeyButton.setText(keyEvent.getCode().toString());
        applyButton.requestFocus();
    }

    /**
     * Checks which language is selected in the propertyloader file.
     * @param propertyLoader
     * @return returns the approperiate index corresponding to languages in list.
     * @throws IOException
     */
    private int checkLanguage(PropertyLoader propertyLoader) throws IOException {
        String temp = propertyLoader.getPropValues("previouslySelectedLanguage");
        if (temp.equals("")){temp = String.valueOf(Locale.getDefault());}
        for (int i = 0;  i < languagecode.length ; i++){
            if (languagecode[i].equals(temp)){
                return i;
            }
        }
        return 0;
    }
}
