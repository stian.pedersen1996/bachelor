package norsvin.gui;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import norsvin.exceptions.alerts.AlertMessage;
import norsvin.logic.logging.MyLogger;
import norsvin.logic.scaleConnect.BluetoothController;
import norsvin.logic.scaleConnect.PropertyLoader;
import norsvin.logic.scaleConnect.ServicesSearch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The type Bluetooth devices scene.
 */
public class BluetoothDevicesScene{

    @FXML
    private ListView<String> deviceList;


    @FXML
    private Label label_bluetoothURL,
            lbl_bluetoothAdress,
            statusConnection;

    @FXML
    private ProgressIndicator progressIcon;


    private String tempurl;
    private boolean connected = false;
    private final BluetoothController bluetoothController = new BluetoothController();
    private Map<Integer, List<String>> mapDevicePosition = new HashMap<>();
    private Map<String, List<String>> mapReturnResult = new HashMap<>();
    private List<String> list;
    /**
     * The Logg.
     */
//Gets logger object
    Logger logg = MyLogger.getLogger();


    /**
     * Initialize.
     */
    public void initialize() {
        list = new ArrayList<>();
        discoverBluetoothDevices();
    }


    /**
     * Get selected device from listview.
     *
     * @param event the event
     */
    @FXML
    void selectedFromList(MouseEvent event) {

        List<String> tmpDeviceDetails = mapDevicePosition.get(deviceList.getSelectionModel().getSelectedIndex());
        try {

            if (tmpDeviceDetails.size() > 2 && tmpDeviceDetails.get(2) != null) {
                String url = tmpDeviceDetails.get(2);
                label_bluetoothURL.setText(url);
                //URL btspp://..............
                tempurl = tmpDeviceDetails.get(2);
                lbl_bluetoothAdress.setText(tmpDeviceDetails.get(1));
            } else {

                AlertMessage.showAlert(
                        "norsvin.scale.title.notSupported",
                        "norsvin.scale.header.notSupported",
                        "norsvin.scale.context.notSupported");
            }
        } catch (Exception e) {
            logg.log(Level.WARNING,"Nothing selected from list, or not supported:  ", e);
        }

    }

    /**
     * Starts bluetooth and discover devices, paired and not paired
     * Discovers bluetooth devices.
     */
    public void discoverBluetoothDevices() {
        final int[] intDevicePosition = {0};
        ServicesSearch ss = new ServicesSearch();

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                //Starts/visible the progress indicator. To indicate that search has started.
                progressIcon.setVisible(true);
                //Search for new bluetooth devices.
                mapReturnResult = ss.getBluetoothDevices();

                for (Map.Entry<String, List<String>> entry : mapReturnResult.entrySet()) {
                    //Only add device with SPP service to list. As SPP service will be the 3rd entry.
                    if(entry.getValue().size() > 2){
                    list.add(entry.getValue().get(0));
                    //Map result to a hashMap.
                    mapDevicePosition.put(intDevicePosition[0], entry.getValue());

                    intDevicePosition[0]++;
                    }
                }
                //Sets result from bluetooth search to gui list.
                deviceList.setItems(FXCollections.observableList(list));
                //Turns of progress indicator, to indicate that search is over.
                progressIcon.setVisible(false);
            }

        });

        thread.start();

    }


    /**
     * Returns weight to norsvincontroller.
     *
     * @param url the url to bluetoothDevice.
     * @return the weight retrieved from the scale.
     * @throws InterruptedException the interrupted exception
     */


    /**
     * Connects to selected bluetooth device from list.
     * Connect to device.
     *
     * @param event the event
     */
    @FXML
    void connectToDevice(MouseEvent event) {
        PropertyLoader propertyLoader = new PropertyLoader();

        try {
            if (tempurl != null) {
                statusConnection.setText("Connected");
                connected = true;
                propertyLoader.setPropValue("previouslySelectedScale",tempurl);

                // get a handle to the stage
                Stage stage = (Stage) statusConnection.getScene().getWindow();
                // do what you have to do
                stage.close();

            } else {
                AlertMessage.showAlert(
                        "norsvin.device.title.connect",
                        "norsvin.device.header.connect",
                        "norsvin.delete.context.connect"
                );
            }
        } catch (Exception e) {
            logg.log(Level.WARNING,"Connecting to device:  ", e);

        }
    }
}
