package norsvin.gui;

import com.fazecast.jSerialComm.SerialPort;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import norsvin.logic.scaleConnect.PropertyLoader;
import norsvin.logic.scaleConnect.RS232Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The type Rs 232 port scene.
 */
public class RS232PortScene {


    /**
     * The .
     */
    int i;
    /**
     * The Connections.
     */
    ArrayList<SerialPort> connections;
    /**
     * The List.
     */
    List<String> list = new ArrayList<String>();
    /**
     * The Rs 232 controller.
     */
    RS232Controller rs232Controller = new RS232Controller();
    @FXML
    private Label statusConnectionRS;
    @FXML
    private ListView<String> portList;
    @FXML
    private Label lbl_sysPortName;
    @FXML
    private Label lbl_portDescription;
    private String tempurl;
    /* Map to get device details list */
    private Map<String, List<String>> mapReturnResult = new HashMap<String, List<String>>();
    /* Map to identify device on user click of JList */
    private Map<Integer, List<String>> mapDevicePosition = new HashMap<Integer, List<String>>();

    /**
     * Initialize.
     */
    public void initialize() {

        list = new ArrayList<String>();
        listCommPorts();
    }

    /**
     * Clicked list.
     *
     * @param event the event
     */
    @FXML
    void ClickedList(MouseEvent event) {
        i = portList.getSelectionModel().getSelectedIndex();
        lbl_sysPortName.setText(connections.get(i).getDescriptivePortName());
        lbl_portDescription.setText(connections.get(i).getPortDescription());

    }


    private void listCommPorts() {
        rs232Controller = new RS232Controller();
        //Should return an arraylist with connections ? //Todo
        connections = RS232Controller.connection();
        ArrayList<String> test = new ArrayList(connections);
        portList.setItems(FXCollections.observableList(test));

    }

    /**
     * Connect to device.
     *
     * @param event the event
     */
//Connects to selected bluetooth device from list.
    @FXML
    void connectToDevice(MouseEvent event) throws IOException {
        //Remove later. Todo
        System.out.println("Skriver ut selection id : " + i);

        PropertyLoader propertyLoader = new PropertyLoader();
        propertyLoader.setPropValue("previouslySelectedPort", connections.get(i).getSystemPortName());
        statusConnectionRS.setText("Connected");
    }

    /**
     * Re search devices.
     *
     * @param event the event
     */
//Research for new devices
    @FXML
    void reSearchDevices(MouseEvent event) {
        //Sets list and hashmap to null again.
        list = new ArrayList<String>();
        mapReturnResult = new HashMap<String, List<String>>();
        /* Map to identify device on user click of list */
        mapDevicePosition = new HashMap<Integer, List<String>>();

        //Does a new service search, of bluetooth devices.
        listCommPorts();
    }

}
