package norsvin.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import norsvin.database.ModelTable;
import norsvin.logic.listener.GlobalListener;
import norsvin.logic.logging.MyLogger;
import norsvin.logic.scaleConnect.PropertyLoader;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The type Norsvin controller.
 */
public class NorsvinController {


    //--------------------------Main scene-------------------------------------------------------------------


    private ObservableList<String> optionScale =
            FXCollections.observableArrayList("Bluetooth Scale", "RS232 Scale");
    /**
     * The Gui controller.
     */
    public GUIRouter guiRouter = new GUIRouter();
    /**
     * The Global listener.
     */
    public GlobalListener globalListener;
    /**
     * The Race list.
     */
//Select race
    ObservableList<String> raceList =
            FXCollections.observableArrayList("LLLL", "DDDD", "ZZZZ", "ZZLL");
    /**
     * The Sex list.
     * \u000C5 = å.
     */
    ObservableList<String> sexList =
            FXCollections.observableArrayList("R\u00E5ne", "Purke", "Kastrat");
    /**
     * The Oblist.
     */
    ObservableList<ModelTable> oblist = FXCollections.observableArrayList();
    /**
     * The Search list.
     */
    ObservableList<ModelTable> searchList = FXCollections.observableArrayList();

    /**
     * The Logg.
     */
//Gets logger object
    Logger logg = MyLogger.getLogger();

    @FXML
    private DatePicker
            datePicker,
            searchDateLow,
            searchDateHigh;

    //--------------------------Tableview------------------------------------------------------------------------
    @FXML
    private ComboBox<String>
            comboLanguage,
            comboBoxRace,
            comboBoxSex,
            comboBoxScale;

    @FXML
    private Circle rs232Icon,
            bluetoothIcon;
    //--------------------------Tab variables ----------------------------------------------------------------
    @FXML
    private VBox rootContainer;
    @FXML
    private TextField textfieldSearch,
            u3TextFieldLocation,
            u3TextFieldDate,
            u3TextFieldWeight,
            u3TextFieldTatonumber,
            textFieldLocation,
            textFieldWeight,
            textFieldAge,
            textFieldTatonumber,
            textfieldSearchLocation,
            textfieldSearchRace,
            textfieldSearchSex,
            textfieldSearchDateLow,
            textfieldSearchDateHigh,
            textfieldSearchWeightLow,
            textfieldSearchWeightHigh,
            textFieldRfid,
            deleteTatonumberGraph;

    //////////////////////////////GRAPH //////////////////////////////////////
    @FXML
    private DatePicker textFieldDate;
    @FXML
    private CheckBox
            checkBoxExported,
            checkBoxDeceased;
    @FXML
    private Button
            exportButton,
            tableSearch,
            importButton,
            btButton,
            rs232Button,
            tono,
            inGris,
            u3Weigh,
            u3AddButton,

    u3DeleteButton,
            u0Weigh,
            u0AddButton,

    u0DeleteButton,
            addToGraph,
            addCurrentToGraph,
            deleteTatoFromGraph;
    @FXML
    private TableView<ModelTable>
            table,
            searchTable;
    @FXML
    private TableColumn<ModelTable, String>
            col_race,
            col_sex,
            col_location,
            col_tatonumber,
            col_date,
            col_age,
            col_weight,
            col_rfid,
            search_col_rfid,
            search_col_weight,
            search_col_date,
            search_col_age,
            search_col_tatonumber,
            search_col_location,
            search_col_sex,
            search_col_race;


    ////////////////////////////////////////////////////////////////
    @FXML
    private LineChart<Number, Number> lineGraph;

    //Line graph axis.
    @FXML
    private NumberAxis xAxis;
    @FXML
    private NumberAxis yAxis;
    public PropertyLoader propertyLoader = new PropertyLoader();
    private LocalDate selectedDate;

    /**
     * Gets all objects in database and lists them in {@link #table}
     *
     * @see GUIRouter#getNotExportedItemsToSession(ObservableList)
     */
    public void setUnexportedToTableView() {
        oblist.clear();
        //Connects to local database and retrieves all objects.
        guiRouter.getNotExportedItemsToSession(oblist);
        //fx:id for tablecoloum.
        col_tatonumber.setCellValueFactory(new PropertyValueFactory<>("tatonumber"));
        col_rfid.setCellValueFactory(new PropertyValueFactory<>("rfid"));
        col_location.setCellValueFactory(new PropertyValueFactory<>("location"));
        col_race.setCellValueFactory(new PropertyValueFactory<>("race"));
        col_age.setCellValueFactory(new PropertyValueFactory<>("age"));
        col_sex.setCellValueFactory(new PropertyValueFactory<>("sex"));
        col_date.setCellValueFactory(new PropertyValueFactory<>("weigh_date"));
        col_weight.setCellValueFactory(new PropertyValueFactory<>("weight"));
        //Sets values to table in gui.
        table.setItems(oblist);
    }

    /**
     * Gets all objects in database and lists them in {@link #table}
     *
     * @see GUIRouter#searchDBWithFilter(ObservableList, String[])
     */
    public void setSearchedObjectsToTableView() {

        //Arraylist, that populate the tableview
        //Clears previous search
        searchList.clear();
        //Connects to local database and retrieves all objects.
        guiRouter.searchDBWithFilter(searchList, getSearchFilter());
        //Sets values to table in gui.
        search_col_tatonumber.setCellValueFactory(new PropertyValueFactory<>("tatonumber"));
        search_col_rfid.setCellValueFactory(new PropertyValueFactory<>("rfid"));
        search_col_location.setCellValueFactory(new PropertyValueFactory<>("location"));
        search_col_weight.setCellValueFactory(new PropertyValueFactory<>("weight"));
        search_col_age.setCellValueFactory(new PropertyValueFactory<>("age"));
        search_col_date.setCellValueFactory(new PropertyValueFactory<>("weigh_date"));
        search_col_sex.setCellValueFactory(new PropertyValueFactory<>("sex"));
        search_col_race.setCellValueFactory(new PropertyValueFactory<>("race"));
        searchTable.setItems(searchList);
    }

    /**
     * retrieves all the values from the filter fields and makes a string array with valid values.
     *
     * @return String array with the search filter.
     */
    public String[] getSearchFilter() {
        return new String[]{textfieldSearch.getText(),
                textfieldSearchLocation.getText(),
                textfieldSearchRace.getText(),
                textfieldSearchSex.getText(),
                String.valueOf(searchDateLow.getValue() == null ? "1800-01-01" : searchDateLow.getValue()),
                String.valueOf(searchDateHigh.getValue() == null ? "9999-01-01" : searchDateHigh.getValue()),
                (textfieldSearchWeightLow.getText().isEmpty() ? String.valueOf(-1) :
                        textfieldSearchWeightLow.getText()),
                (textfieldSearchWeightHigh.getText().isEmpty() ? String.valueOf(100000) :
                        textfieldSearchWeightHigh.getText()),
                (checkBoxExported.isSelected() ? "1" : "0"),
                (checkBoxDeceased.isSelected() ? "1" : "0")};
    }


    /**
     * Initialization method for main FXML scene, will update the gui where further initialization is needed.
     */
    @FXML
    private void initialize() throws IOException {
        //Set up table value combobox.
        comboBoxRace.setItems(raceList);
        comboBoxSex.setItems(sexList);
        setUnexportedToTableView();

        //Populate combobox with language list.
        guiRouter.setDateFormat(datePicker);
        guiRouter.setDateFormat(searchDateLow);
        guiRouter.setDateFormat(searchDateHigh);
        textFieldAge.setDisable(true);
        keyLoggerinit();
    }

    /**
     * Reloads the gui, used when language pack is changed.
     *
     * @throws IOException the io exception
     */
    public void reload(PropertyLoader propertyLoader) throws IOException {

        ResourceBundle bundle = ResourceBundle.getBundle("i18n.Language",
                Locale.forLanguageTag(propertyLoader.getPropValues("previouslySelectedLanguage")));
        //Load scene with selected language.
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("/gui/Main" + ".fxml")), bundle);
        Stage window = (Stage) rootContainer.getScene().getWindow();
        window.setScene(new Scene(root));
    }


    private void keyLoggerinit() throws IOException {
        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            logg.log(Level.WARNING, "Key logger init: ", e);
        }
        GlobalScreen.addNativeKeyListener(globalListener = new GlobalListener(
                this));
    }


    /**
     * Method searches for objects in the database
     *
     * @param event gets called when {@link #tableSearch} has been clicked
     * @see #setSearchedObjectsToTableView() #setSearchedObjectsToTableView()for more info.
     */
    @FXML
    void searchDatabase(MouseEvent event) {
        setSearchedObjectsToTableView();
    }


    /**
     * Sets text fields to be the same as the selected object.
     *
     * @param event Method gets triggered when an object in {@link #searchTable} gets clicked.
     */
    @FXML
    void getSelected(MouseEvent event) {
        String[] temp;
        if (event.getSource().equals(searchTable)) {
            table.getSelectionModel().clearSelection();
            temp = guiRouter.getSelectedRow(searchTable);
        } else {
            searchTable.getSelectionModel().clearSelection();
            temp = guiRouter.getSelectedRow(table);
        }
        textFieldTatonumber.setText(temp[0]);
        textFieldRfid.setText(temp[1]);
        if (temp[3]!=null){selectedDate = LocalDate.parse(temp[3]);}
        textFieldAge.setText(temp[4]);
        textFieldLocation.setText(temp[5]);
        comboBoxSex.getSelectionModel().select(temp[6]);
        comboBoxRace.getSelectionModel().select(temp[7]);
        datePicker.setValue(LocalDate.parse(temp[8]));
    }


    /**
     * Weigh.
     * this class should only be used for instigating methods, collecting and setting data
     * {@link #getSelected(MouseEvent)}.
     *
     * @param mouseEvent the mouse event
     * @throws InterruptedException the interrupted exception
     * @throws IOException          the io exception
     */
    @FXML
    void weigh(MouseEvent mouseEvent) throws InterruptedException, IOException {
        textFieldWeight.setText(guiRouter.weigh(propertyLoader));
    }

    /**
     * Used for adding objects into the Database
     * Refreshes both tables after the object has been added.
     *
     * @param mouseEvent called when {@link #u0AddButton} is clicked.
     * @throws SQLException the sql exception
     * @see GUIRouter#addToDB(String, String, String, String, String, LocalDate, String)
     */
    @FXML
    void addToList(MouseEvent mouseEvent) throws SQLException {
        guiRouter.addToDB(
                textFieldTatonumber.getText(),
                textFieldRfid.getText(),
                textFieldLocation.getText(),
                comboBoxSex.getValue(),
                comboBoxRace.getValue(),
                datePicker.getValue(),
                textFieldWeight.getText()
        );
        setUnexportedToTableView();
        setSearchedObjectsToTableView();
    }

    /**
     * Used for deleting objects from the Database
     * Refreshes both tables after the object has been added.
     *
     * @param event called when {@link #u0DeleteButton} is clicked.
     * @see GUIRouter#deleteFromDB(String, LocalDate) GUIController#deleteFromDB(String, LocalDate)
     */
    @FXML
    void deleteFromList(MouseEvent event) {
        guiRouter.deleteFromDB(textFieldTatonumber.getText(), selectedDate);
        //Refreshes table view after object is deleted.
        setUnexportedToTableView();
        setSearchedObjectsToTableView();
    }


    /**
     * Exports unexported objects to a csv file.
     *
     * @param event the event
     */
    @FXML
    void exportToCSV(MouseEvent event) throws IOException {
        if (guiRouter.exportSession(rootContainer)) {
            setUnexportedToTableView();

        }
    }

    /**
     * Imports new objects into the database, using a csv file.
     *
     * @param mouseEvent the mouse event
     * @throws IOException  the io exception
     * @throws SQLException the sql exception
     */
    @FXML
    void importFromCsv(MouseEvent mouseEvent) throws IOException, SQLException {
        if (guiRouter.importSession(rootContainer)) {
            setUnexportedToTableView();
        }
    }


    /**
     * Called every time there is a key event
     * Used as a tool to identify the origin of a keypress and use correlated method for a keypress.
     *
     * @param keyEvent contains the object that initialized the event.
     */
    public void keyListener(KeyEvent keyEvent) {

        if (keyEvent.getSource() == textfieldSearch) {
            if (keyEvent.getCode().toString().equals("ENTER")) {
                setSearchedObjectsToTableView();
            }
            return;
        }
    }

////////////////////////////// RS232 ///////////////////////////////////////


    /**
     * Connect to rs 232.
     *
     * @param actionEvent the action event
     * @throws IOException the io exception
     */
    public void connectToRS232(ActionEvent actionEvent) throws IOException {

        guiRouter.createRS232DeviceScene();
    }


////////////////////////////// Bluetooth ///////////////////////////////////


    /**
     * opens bluetooth connection scene.
     *
     * @param actionEvent the action event
     * @throws IOException the io exception
     */
//Button for connecting to bluetooth device.
    public void connectToBT(ActionEvent actionEvent) throws IOException {
        guiRouter.createBluetoothDeviceScene();
    }


    //////////////////////////////GRAPH //////////////////////////////////////

    /**
     * Clear graph.
     *
     * @param event the event
     */
    @FXML
    void clearGraph(MouseEvent event) throws IOException {

        lineGraph.getData().clear();
    }



    @FXML
    void setAddToGraph(ActionEvent actionEvent) {
        guiRouter.addToGraph(xAxis, yAxis,  lineGraph, searchTable);
    }


////////////////////Settings///////////////////////////


    public void menuPreferences(ActionEvent actionEvent) throws IOException {
        guiRouter.openPreferences(rootContainer);
    }
}