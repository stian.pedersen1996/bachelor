package norsvin.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import norsvin.database.LocalDatabase;
import norsvin.exceptions.alerts.AlertMessage;
import norsvin.logic.logging.MyLogger;
import norsvin.logic.scaleConnect.PropertyLoader;
import org.jnativehook.GlobalScreen;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * The type Main.
 */
public class Main extends Application {


    /**
     * The starting method of the applicaiton.
     * This method will first attempt to connect to a database and will create one if no database is found.
     * Check {@link LocalDatabase#makeNewDB()} for more information.
     * <p>
     * Then the {@link #launch(String...)} method will be initialized,
     * this method will eventually use the {@link #start(Stage)} method.
     *
     * @param args Mandatory main parameter, not used for anything.
     * @throws Exception the exception
     */
    public static void main(String[] args) throws Exception {
        //Launches the application.
        launch(args);
        //Stops the application
        System.exit(0);
    }


    /**
     * Initializes the application before launching the GUI.
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) throws IOException {
        // Gets logger for exceptions
        Logger logg = MyLogger.getLogger();
        // Log most everything
        logg.setLevel(Level.FINE);
        logg.setUseParentHandlers(false);        // Stops console log
        // Set up a new file handler
        FileHandler fh = null;
        try {
            fh = new FileHandler("./logg.log");
            logg.fine("Handler -> MemoryHandler, FileHandler, ConsoleHandler, SocketHandler");
            fh.setLevel(Level.INFO);
        } catch (SecurityException e) {
            AlertMessage.showAlert("Logging exception", "Problem with logging", e.getMessage());
        } catch (IOException e) {
            AlertMessage.showAlert("Logging exception", "Problem with logging", e.getMessage());
        }
        // Set up an Simple formatter, easy to read.
        SimpleFormatter sfr = new SimpleFormatter();
        logg.fine("Formatter -> SimpleFormatter, XMLFormatter");
        // Set the formatter for the log handler
        assert fh != null;
        fh.setFormatter(sfr);
        // add the log handler to the logger
        logg.addHandler(fh);

        try {

            PropertyLoader propertyLoader = new PropertyLoader();

            Logger logger = Logger.getLogger(GlobalScreen.class.getPackage().getName());
            logger.setLevel(Level.WARNING);
            logger.setUseParentHandlers(false);
            LocalDatabase.makeNewDB();


            Locale initLanguage = Locale.forLanguageTag(propertyLoader.getPropValues("previouslySelectedLanguage"));
            System.out.println("Language pack:" + initLanguage);
            if (initLanguage.equals("")){initLanguage = Locale.getDefault();}

            // The primary stage.
            //Language bundle, i18n. Finds users pref. language
            ResourceBundle bundle = ResourceBundle.getBundle("i18n.Language", initLanguage);
            //Load scene with selected language.
            //Loads the FXML GUI files.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/gui/Main.fxml"), bundle);
            VBox root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.getIcons().add(new Image("img/N_icon_logo.png"));
            primaryStage.setTitle(bundle.getString("norsvin.title"));
            primaryStage.show();


        } catch (Exception e) {
            throw e;
        }
    }
}
